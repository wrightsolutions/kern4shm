#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from collections import OrderedDict
from os import getenv as osgetenv
from os import linesep
from sys import exit,argv
import re
import shlex
from subprocess import Popen,PIPE
#import subprocess

# Helper for sysctl queries / conf usually related to kernel. and
# typically shared memory / ipcs related such as those that can
# be checked using ipcs -lm
# The shmmax value is set in 'bytes' (shmmax). However all value is set in 'pages' (shmall).
#   python ./kern4sharedctl.py 'kernel'
# If your server has 32GB total memory then supply 32 as final argument as shown next:
#   python ./kern4sharedctl.py 'kernel' 32
# Above generates in /tmp/ a new 30-postgresql-shm.conf with chunks set as quarter of 64GB
# and shmall set as half of 64 GB (32GB). Maximum size of any shared memory chunk is shmmax.
# shmmax (chunk max) can be much smaller than shmall, but for postgresql usually half of it.


# chr(58) is colon (:)	; chr(61) is equals (=)	; chr(41) is closing parenth ())
# chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)  
SPACER=chr(32)
HASHER=chr(35)
COLON=chr(58)
EQUALS=chr(61)
DOTTY=chr(46)
#DOUBLE_DOT=chr(46)*2
TABBY=chr(9)
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)

re_reading_key_complaint = re.compile("{0}\s+reading\s+key".format(COLON))
re_name_equals_word = re.compile("[a-zA-z0-9\.\-]+\s+{0}\s+\w+".format(EQUALS))
re_space_equals_space = re.compile("{0}{1}{0}".format(SPACER,EQUALS))

BUFSIZE=2048		#BUFSIZE=1024
BINSYSCTL='/sbin/sysctl'
TWO_TO_TEN=2**10
TWO_TO_TWENTY=2**20
TWO_TO_THIRTY=2**30  # Not too far from 10**9 so bytes*TWO_TO_THIRTY gives approx. GB
dict_two_to = OrderedDict({TWO_TO_TEN: 'KB',
TWO_TO_TWENTY: 'MB',
TWO_TO_THIRTY: 'G'}
)

""" Next value is 4096 and hardcoded, but should be applicable for most typical
vanilla GNU/Linux installs. User can check local system using: getconf PAGE_SIZE
"""
PAGESIZE_DEFAULT=4*TWO_TO_TEN	# 4096

CTLD_OUTFILE='/tmp/30-postgresql-shm.conf'

DEBIAN_OLDCONF="""
kernel.shmmax = 33554432
# Force segment size (shmmax) to be 2GB? /sbin/sysctl -w kernel.shmmax=2147483648
kernel.shmall = 2097152
# 8GB for shmall*PAGE_SIZE looks like 2097152*PAGE_SIZE typically
kernel.shmmni = 4096
# shmmni is (max) number of shared memory segments
"""


def before_equals(equals_string):
	before = ''
	try:
		before = equals_string.split(EQUALS, 1)[0]
	except:
		pass
	if len(before) > 0:
		before = before.strip()
	return before


def after_equals(equals_string):
	after = ''
	try:
		after = equals_string.split(EQUALS, 1)[1]
	except:
		pass
	if len(after) > 0:
		after = after.lstrip()
	return after


def after_int(equals_string,nonint=None):
	after = after_equals(equals_string)
	if len(after) < 1:
		return nonint
	try:
		a_int = int(after)
	except:
		a_int = nonint
	return a_int


def sub5(sub_command,verbosity=0):
	""" Execute subprocess popen for the given command
	Return a 5 tuple of stdout as iterator, stdout,
	stderr, stderr as iterator, and return code.
	"""
	cmdlex = shlex.split(sub_command)
	subbed = Popen(cmdlex,bufsize=BUFSIZE,stdout=PIPE,stderr=PIPE)
	subout, suberr = subbed.communicate()
	# .returncode is only populated after a communicate() or poll/wait
	subrc = subbed.returncode
	if verbosity > 1 and subrc > 2:
		print("Error {0} during {1}".format(ncon_rc,cmd_ncon))
	elif verbosity > 2:
		if subrc > 0:
			print("subrc={0} from .returncode={1}".format(subrc,subbed.returncode))
	else:
		pass
	subout_iter = iter(subout.splitlines())
	try:
		suberr_iter = iter(suberr.splitlines())
	except AttributeError:
		suberr_iter = iter([])
	return (subout_iter,subout,suberr,suberr_iter,subrc)


def suffixed_from_int(int_undivided,unitdiv,udesc=None):
	""" String suffixes that indicate something not quite right:
	undivided
	suffix unallocated
	suffix unallocated (bad divisor)
	suffix unallocated (bad pagesize)
	"""
	str = "{0} undivided".format(int_undivided)
	if unitdiv is None or unitdiv < 1:
		return str
	if 'pages' == udesc:
		suffix = udesc
	else:
		suffix = 'suffix unallocated'

	if 1 == unitdiv:
		# Attempt our best human form
		suffix = 'byte human best'
		#print(suffix,suffix)
		#suffix = 'byte'
		for intkey,unitdesc in dict_two_to.items():
			if int_undivided < intkey:
				continue
			unitdiv = intkey
			suffix = unitdesc
		# smallest entry in dict_two_to equates to KB
	elif udesc is None or udesc == '':
		suffix = 'unitdiv divisor based'
		if TWO_TO_TEN == unitdiv:
			suffix = 'KB'
		elif TWO_TO_TWENTY == unitdiv:
			suffix = 'MB'
		elif TWO_TO_THIRTY == unitdiv:
			suffix = 'G'
		elif 'pages' == udesc:
			suffix = udesc
		else:
			pass
	else:
		pass

	try:
		unit_int = int(unitdiv)
	except:
		unit_int = 0
		if 'pages' == suffix:
			suffix = 'suffix unallocated (bad pagesize)'
		else:
			suffix = 'suffix unallocated (bad divisor)'

	int_new = int_undivided
	if unit_int > 0:
		try:
			int_new = int_undivided//unit_int
		except:
			suffix = 'suffix unallocated (bad int result)'
	str = "{0} {1}".format(int_new,suffix)
	return str


def predot2dict(predot,iter_lines,maxlines=500,verbosity=0):
	""" From provided lines iterator, populate (and return) a
	dictionary of key value with value being integer wherever possible
	First argument is used as a filter on the lines and only those lines
	that start with dot prefixed with string from predot will be considered
	valid for entry into the dictionary.
	"""
	predot_dict = OrderedDict()
	if maxlines < 1:
		if verbosity > 2:
			print("maxlines={0} means exit neededby_list_populate".format(maxlines))
		return []
	idx = 1
	re_predot = re.compile("\s*{0}{1}".format(predot,DOTTY))
	while idx <= maxlines:
		try:
			line = iter_lines.next()
			if re_predot.match(line):
				if verbosity > 2:
					print('match(line) found required predot')
				#needed_name = line.split()[-1]
				a_int = after_int(line)
				if a_int is None:
					if verbosity > 2:
						print("noninteger line: {0}".format(line))
				elif 0 == a_int:
					if verbosity > 2:
						print("integer ZERO valued line: {0}".format(line))
				else:
					if verbosity > 2:
						print("integer line: {0}".format(line))
				pre = before_equals(line)
				predot_dict[pre]=a_int
				# end of match() processing

		except StopIteration:
			if verbosity > 1:
				print("{0} breaking out when idx={1}".format(predot,idx))
			break
		idx+=1

	return predot_dict


def predot_dict_lines(pdict,printer=False,comment_level=0):
	""" Iterate (and print when requested) the predot dictionary
	printer=True for printing of output but lines are returned as
	a list if the caller prefers False and handle printing themselves.
	comment_level=0 means do not add any comments inline or otherwise
	to the contents supplied.
	comment_level=1 means add sparse helper comments.
	comment_level=2 is typical and means add comments which help
	by showing human friendly translations for the numbers
	"""
	predot_lines = []

	for key,val in pdict.items():
		pline = "{0} {1} {2}".format(key,EQUALS,val)
		predot_lines.append(pline)
		if comment_level < 1:
			continue
		cpref = "{0} {1}{2}{0} ".format(HASHER,pline,TABBY,HASHER)
		if 'shmmax' in key:
			#cpref = "{0} {1}{2}{0} ".format(HASHER,pline,TABBY,HASHER)
			if comment_level > 1:
				if val > (512*TWO_TO_THIRTY):
					str = suffixed_from_int(val,TWO_TO_THIRTY)
				else:
					#str = suffixed_from_int(val,TWO_TO_TWENTY)
					# Human best by supplying ,1,None
					str = suffixed_from_int(val,1,None)
				cline = "{0}MAXSEGSIZE approx. {1}".format(cpref,str)
				predot_lines.append(cline)
			else:
				predot_lines.append(cpref+'MAXSEGSIZE')
			""" SHMMAX is set in <uapi/linux/shm.h> and for 64 bit
			systems equals the default value 18446744073692774399
			"""
		elif 'shmall' in key:
			# PAGESIZE_DEFAULT=4096 is hardcoded in our script
			pagesize = PAGESIZE_DEFAULT
			#cpref = "{0} {1}{2}{0} ".format(HASHER,pline,TABBY,HASHER)
			if comment_level > 1:
				val4 = val*pagesize
				str = suffixed_from_int(val4,1,None)
				if val > (512*TWO_TO_THIRTY):
					pass
				else:
					pass
				cline = "{0}ALL approx. {1}".format(cpref,str)
				predot_lines.append(cline)
			else:
				predot_lines.append(cpref+'ALL')
		elif 'shmmni' in key:
			# System-wide limit on the number of shared memory segments
			if comment_level > 1:
				#str = suffixed_from_int(val,TWO_TO_TEN)
				cline = cpref+'Number of segments limit'
				predot_lines.append(cline)
			else:
				predot_lines.append(cpref+'NUMSEG')
		else:
			pass

	if printer is True:
		for line in predot_lines:
			print(line)

	return predot_lines


def ctld_conflines(dict_supplied,maxgig,lines_tail=[]):
	""" Write out a replacement/new postgresql-shm.conf based on 
	maxgig supplied (maxgig here represents physically installed memory in GB)

	Please choose a value for SHMALL so smaller than free RAM to avoid paging !!

	For easy reference showing 4K based page 'shmall' figures next:
	8GB	2097152
	16GB	4194304
	24GB	6291456
	32GB	8388608
	# Above are the kind of numbers you might see in a reasonably
	# limited shmall. For shmmax a smallish 32MB would be 33554432
	"""
	conf_lines = []

	# PAGESIZE_DEFAULT=4096 is hardcoded in our script
	pagesize = PAGESIZE_DEFAULT

	try:
		installed_memory_in_pages = (TWO_TO_THIRTY*int(maxgig))//pagesize
		all_new_pages = installed_memory_in_pages/2
		# Above we are saying shmall to be half of the total installed memory
		shmmax_new = (TWO_TO_THIRTY*int(maxgig))//4
	except:
		all_new_pages = None
		shmmax_new = None

	for key,val in dict_supplied.items():
		line = "{0} {1} {2}".format(key,EQUALS,val)

		eqval = None
		if 'shmall' in key and all_new_pages is not None:
			""" 8GB for shmall*PAGE_SIZE looks like 2097152*4096 typically
			16GB for shmall*PAGE_SIZE looks like 4194304*4096 typically
			32GB for shmall*PAGE_SIZE looks like 8388608*4096 typically
			"""
			eqval = "{0} {1}".format(EQUALS,all_new_pages)
		elif 'shmmax' in key and shmmax_new is not None:
			eqval = "{0} {1}".format(EQUALS,shmmax_new)
			tail_shmmax1 = "For a 4K paged system, {0}".format(shmmax_new)
			gig_approx = shmmax_new//TWO_TO_THIRTY
			#print(gig_approx,gig_approx)
			tail_shmmax2 = "shmmax above equates to {0} G".format(gig_approx)
			# Next prepend concatenation of 1 and 2 to lines_tail
			lines_tail.insert(0, "# {0} {1}".format(tail_shmmax1,tail_shmmax2))
			""" Remember that 32MB for shmmax is 33554432 and because shmmax
			is bytes the numbers always look big
			"""
		elif 'shmmni' in key:
			# shmmni - System-wide limit on the number of shared memory segments
			pass
		else:
			# No New value so contents of line already good
			pass

		if eqval is not None:
			# We are making a replacement. Set line here.
			line = "{0} {1}".format(key,eqval)
		conf_lines.append(line)

	# Next we iterate lines_tail and keep the commented lines
	for line in lines_tail:
		if not line.startswith(HASHER):
			# Uncommented so skip the line
			continue
		# Line is commented so safe to add as tail
		conf_lines.append(line)
	
	return conf_lines



if __name__ == '__main__':

	program_binary = argv[0].strip()
	from os import path
	program_dirname = path.dirname(program_binary)

	maxgig = None
	try:
		predot = argv[1]
		try:
			maxgig = argv[2]
		except IndexError:
			pass
	except IndexError:
		predot = 'predotty'

	PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
	PYVERBOSITY = 1
	if PYVERBOSITY_STRING is None:
		pass
	else:
		try:
			PYVERBOSITY = int(PYVERBOSITY_STRING)
		except:
			pass
	#print(PYVERBOSITY)

	""" Next verbosity_global = 1 is the most likely outcome
	unless the user specifically overrides it by setting
	the environment variable PYVERBOSITY
	"""
	if PYVERBOSITY is None:
		verbosity_global = 1
	elif 0 == PYVERBOSITY:
		# 0 from env means 0 at global level
		verbosity_global = 0
	elif PYVERBOSITY > 1:
		# >1 from env means same at global level
		verbosity_global = PYVERBOSITY
	else:
		verbosity_global = 1


	sub_cmd = "{0} -a --pattern kernel.shm[a-z]".format(BINSYSCTL)
	subout_iter,subout,suberr,suberr_iter,subrc = sub5(sub_cmd,0)
	pdict = predot2dict('kernel',subout_iter,500,verbosity_global)
	# Above fixes 'kernel' for predot rather than whatever supplied as arg
	#pdict = predot2dict(predot,subout_iter,500,verbosity_global)


	if verbosity_global > 0:
		plines = predot_dict_lines(pdict,printer=True,comment_level=2)

	if maxgig is None:
		exit(subrc)

	""" All that follows now concerns preparation and act of writing to
	a temporary area a new postgresql-shm.conf that might be copied into
	place in /etc/sysctl.d/ if user validated.
	"""
	lines_hashed = [ str(ln) for ln in plines if ln.startswith(HASHER) ]
	clines = ctld_conflines(pdict,maxgig,lines_hashed)

	""" Usually the file writing next is non-destructive in that we
	are writing to /tmp/ or other temporary area.
	Copying the resulting conf into place is a user choice / manual act.
	"""
	with open(CTLD_OUTFILE, 'w') as cfile:
		for cline in clines:
			cfile.write("{0}{1}".format(cline,linesep))

	exit(subrc)

