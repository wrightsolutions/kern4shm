# Postgresql shared memory and kernel settings

This repo contains helpers for commenting / generating
conf files with suitable shmmax and shmall



## Postgresql 9.4 and newer

Postgresql uses a newer method of shared memory allocation and
unlike previous versions, is less bothered about kernel settings such
as kernel.shmmax



## max_connections and shared_buffers

Further discussed in repo 'postgres92and91conf'

shared_buffers is set initially from a best guess during 'initdb'

chgrp postgres /home/data/;chmod 2775 /home/data/;
su -c "/usr/bin/initdb --locale=C /home/data/pg92" postgres





## Historical error message text for reference

'This error usually means that PostgreSQL's request for a shared memory 
segment exceeded your kernel's SHMMAX parameter'

cent7bit64updated20181213 pg_ctl[4333]: FATAL:  could not create shared memory segment: Cannot allocate memory
cent7bit64updated20181213 pg_ctl[4333]: DETAIL:  Failed system call was shmget(key=5432001, size=1123164160, 03600).
cent7bit64updated20181213 pg_ctl[4333]: HINT:  This error usually means that PostgreSQL's request for a shared memory segment exceeded available memory


## 16G server and testing threshold of sharedbuffers (postgresql)

```
kernel.shmall = 2097152
# kernel.shmall = 2097152	# ALL approx. 8 G
kernel.shmmax = 4294967296
# kernel.shmmax = 4294967296	# MAXSEGSIZE approx. 4 G
```

If your settings look like above (shmall is half of RAM), then certainly 3G of shared_buffers will be fine here.
That 3GB shared_buffers could be obtained using helper as shown:
postgresql92conf.py --timezone 'UTC' --sharedbuffers 3GB  --contrib statements

