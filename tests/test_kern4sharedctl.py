#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import TestCase
from os import linesep
import re
from string import ascii_lowercase,digits,printable,punctuation

import kern4sharedctl as shar

class InputDesktop(TestCase):

	def setUp(self):

		# chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
		# chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)
		# chr(45) is hyphen			; chr(64) is at symbol (@)
		# chr(32) is space ; chr(10) is line feed

		self.SET_COLON_SPACE_EQUALS = set([chr(58),chr(32),chr(61)])
		self.SET_PRINTABLE = set(printable)
		self.SET_PUNCTUATION = set(punctuation)
		self.SET_LOWER_PLUS_DIGITS = set(ascii_lowercase).union(digits)

		self.STUFF1 = 'Some stuff'
		self.STUFF2 = 'More stuff'
		self.STUFF3 = 'Even more stuff'

		self.HEAD2NEWLINES = "{0}{0}".format(chr(10))
		self.HEAD2NEWLINES_SPLIT = ['','']
		#
		self.TAIL2NEWLINES = "{0}{1}{0}{1}".format(chr(32),chr(10))
		self.TAIL2NEWLINES_SPLIT = [' ',' ']
		#

		self.LARGEBREAK = 18446744073692774399
		""" Above value is tending to appear in place of meaningful limits where
		the purpose of the Linux server is not know in advance or no tailoring
		of purpose is appropriate. Change first proposed for kernel 3.16 in 2014.
		'Starting with the 3.16 kernel, the default limits for both of those [shm] values
		are raised to ULONG_MAX - 2^24 in the kernel already.'
		https://bugzilla.redhat.com/show_bug.cgi?id=1150130
		"""

		self.NUMSEG4096INSTALLED8 = """
kernel.shmmax = 33554432
kernel.shmall = 1048576
kernel.shmmni = 4096
"""
		self.NUMSEG4096INSTALLED8_SPLIT = self.NUMSEG4096INSTALLED8.strip().split(linesep)

		self.NUMSEG4096INSTALLED16 = """
kernel.shmmax = 33554432
kernel.shmall = 2097152
kernel.shmmni = 4096
"""
		self.NUMSEG4096INSTALLED16_SPLIT = self.NUMSEG4096INSTALLED16.strip().split(linesep)

		self.PAGE4_TEMPLATE = """
kernel.shmmax = {0}
kernel.shmall = {1}
kernel.shmmni = 4096
"""

		self.PAGE4LARGEBREAK = (self.PAGE4_TEMPLATE.format(self.LARGEBREAK,self.LARGEBREAK)).strip()
		self.PAGE4LARGEBREAK_SPLIT = self.PAGE4LARGEBREAK.split(linesep)

		# Next we have a commented out section useful if you want a copy of test data to /tmp/
		"""
		with open('/tmp/testdata.txt','w') as f:
			for line in self.PAGE4LARGEBREAK_SPLIT:
				f.write("{0}\n".format(line))
			f.flush()
		# Copy of above self.PAGE4LARGEBREAK_SPLIT is saved in input/page4largebreak.txt
		"""
		return


	def test_human(self):
		""" Here we are testing the selection of appropriate
		best human unitdesc entry from dictionary dict_two_to
		# kernel.shmall = 2097152\t# ALL approx. 8589934592 ???
		"""
		pagesize = shar.PAGESIZE_DEFAULT	# 4096
		val4 = pagesize*(2**21)
		str8 = shar.suffixed_from_int(val4,1,None)
		#print(str8)
		self.assertEqual(str8,'8 G')
		return



	def test_numseg4096largebreak(self):
		""" Test of correctly commenting page4largebreak settings
		PAGE4LARGEBREAK involves the typically big numbers like 18446744073692774399
		"""
		self.assertEqual(len(self.PAGE4LARGEBREAK_SPLIT),3)
		iter_test = iter(self.PAGE4LARGEBREAK_SPLIT)
		#predot2dict(predot,iter_lines,maxlines=500,verbosity=0)
		predot_dict = shar.predot2dict('kernel',iter_test,500,0)
		self.assertEqual(len(predot_dict),3)
		plines = shar.predot_dict_lines(predot_dict,False,comment_level=2)
		self.assertEqual(len(plines),6)
		#print(plines)
		#self.assertEqual(len(noutput_filtered_on_warning),2)
		return


	def test_numseg4096installed8conflines(self):
		""" Test of correctly generating conflines similar to
		numseg4096installed8 settings
		"""
		self.assertEqual(len(self.NUMSEG4096INSTALLED8_SPLIT),3)
		iter_test = iter(self.NUMSEG4096INSTALLED8_SPLIT)
		#predot2dict(predot,iter_lines,maxlines=500,verbosity=0)
		predot_dict = shar.predot2dict('kernel',iter_test,500,0)
		self.assertEqual(len(predot_dict),3)
		plines = shar.predot_dict_lines(predot_dict,False,comment_level=2)
		#print(plines)
		self.assertEqual(len(plines),6)
		line32 = '# kernel.shmmax = 33554432\t# MAXSEGSIZE approx. 32 MB'
		self.assertTrue(line32 in plines)
		line4 = 'kernel.shmall = 1048576'	# New setting requested
		self.assertTrue(line4 in plines)
		clines = shar.ctld_conflines(predot_dict,8,plines)
		line2 = '# For a 4K paged system, 2147483648 shmmax above equates to 2 G'
		self.assertEqual(len(clines),7)
		self.assertTrue(line2 in clines)
		#self.assertEqual(len(noutput_filtered_on_warning),2)
		return


	def test_numseg4096installed16alreadygood(self):
		""" Test of correctly commenting numseg4096installed16 settings
		assuming system is already set with those values
		"""
		self.assertEqual(len(self.NUMSEG4096INSTALLED16_SPLIT),3)
		iter_test = iter(self.NUMSEG4096INSTALLED16_SPLIT)
		#predot2dict(predot,iter_lines,maxlines=500,verbosity=0)
		predot_dict = shar.predot2dict('kernel',iter_test,500,0)
		self.assertEqual(len(predot_dict),3)
		plines = shar.predot_dict_lines(predot_dict,False,comment_level=2)
		self.assertEqual(len(plines),6)
		#print(plines)
		#self.assertEqual(len(noutput_filtered_on_warning),2)
		return


	def test_numseg4096installed16conflines(self):
		""" Test of correctly generating conflines similar to
		numseg4096installed16 settings
		"""
		self.assertEqual(len(self.NUMSEG4096INSTALLED16_SPLIT),3)
		iter_test = iter(self.NUMSEG4096INSTALLED16_SPLIT)
		#predot2dict(predot,iter_lines,maxlines=500,verbosity=0)
		predot_dict = shar.predot2dict('kernel',iter_test,500,0)
		self.assertEqual(len(predot_dict),3)
		plines = shar.predot_dict_lines(predot_dict,False,comment_level=2)
		#print(plines)
		self.assertEqual(len(plines),6)
		line32 = '# kernel.shmmax = 33554432\t# MAXSEGSIZE approx. 32 MB'
		self.assertTrue(line32 in plines)
		line4 = 'kernel.shmall = 2097152'	# New setting requested
		self.assertTrue(line4 in plines)
		line8commented = '# kernel.shmall = 2097152\t# ALL approx. 8 G'
		self.assertTrue(line8commented in plines)
		clines = shar.ctld_conflines(predot_dict,16,plines)
		line4 = '# For a 4K paged system, 4294967296 shmmax above equates to 4 G'
		self.assertEqual(len(clines),7)
		self.assertTrue(line4 in clines)
		#self.assertEqual(len(noutput_filtered_on_warning),2)
		return




